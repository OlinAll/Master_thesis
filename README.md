This is a repository with the files from the EMSD Master thesis submitted as in partial fulfilment of the requirements of Master of Philosophy in System Dynamics (Universitetet i Bergen), Master of Science in System Dynamics (Universidade NOVA de Lisboa), and Master of Science in Business Administration (Radboud Universiteit Nijmegen). Supervised by prof. Birgit Kopainsky (UiB) and prof. Paulo Gonçalves (USI)

ABSTRACT

In such areas like infectious diseases management, resource allocation strategies are
crucial, as they determine the livelihoods of people both in the short-term and in
the long-term. Often, a specific geographic area is affected by several diseases that
create a heavy burden on the society. As resources are usually limited, allocating
resources, for example, to fund vaccination activities, becomes highly important and
full of trade-offs. With managing infectious diseases there are multiple decisions that
need to take place, the results of which will affect the situation in the future. Being
demanding in general, dynamic decision making in such a domain becomes even
more complex as there are people’s lives at stake. Arriving at the understanding
of how people will behave in such situations through field experiments is not only
costly but also unethical. System dynamics-based simulators proved to be fruitful in
the experimental research in dynamic decision making. The novelty of this research
is in its application of a system dynamics-based simulator in an experimental setting
to investigate the general strategic choices people make when they need to eradicate
two competing diseases with a limited financial resource. In addition, the research
strives to shed light on the possible reasons to why people do not pursue eradication
strategy. The results provide an empirical evidence on the four general clusters of
allocation strategies, with the majority of subjects who did not pursue eradication.
One of the explanations for such performance is that even in the sparing conditions
of the game, the context of the problem was too demanding for the subjects to
correctly infer the results of their actions, use the provided information and based
on that infer which strategy would lead to eradication. Further research should
concentrate on assessing subjects’ understanding of the underlying system and its
effect on their decisions as well as testing a broader range of dynamic decisions in
the simulator.